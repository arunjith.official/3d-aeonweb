// Import required modules
import express from 'express';
import OpenAI from 'openai';
import dotenv from 'dotenv';

// Load environment variables from .env file
dotenv.config();

const router = express.Router();

// Initialize the OpenAI instance with your secret key
const openai = new OpenAI(process.env.OPENAI_API_KEY);

// Define the route for generating DALL-E images
router.post('/generate-image', async (req, res) => {
  try {
    // Extract parameters from the request body
    const { prompt } = req.body;

    // Define the parameters for image generation
    const params = {
      prompt: prompt,
      n: 1,
      size: "1024x1024",
      response_format: 'b64_json'
    };

    // Send the request to generate the image
    const response = await openai.images.generate(params);

    // Extract the image URL from the response
    const image = response.data[0].b64_json

    res.status(200).json({ photo: image });
  } catch (error) {
    console.error("Error generating image:", error);
    res.status(500).json({ message: "Something went wrong" });
  }
});

// Export the router
export default router;
