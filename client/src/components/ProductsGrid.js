export default function ProductGrid({ products }) {
    return (
        <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-10">
            {products ? products.map(product => (
                <ProductBox key={product._id} {...product} />
            )) : null}
        </div>
    );
}
