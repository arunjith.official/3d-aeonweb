import React from "react";
import CostomButton from "./CostomButton";
import { getContrastingColor } from "../config/helpers";

import state from "../store";
import { useSnapshot } from "valtio";

const FilePicker = ({ file, setFile, readFile }) => {
  const snap = useSnapshot(state);
  const textStyle = {
    color: getContrastingColor(snap.color),
  };
  return (
    <div className="filepicker-container">
      <div className="flex-1 flex flex-col">
        <input
          id="file-upload"
          type="file"
          accept="image/*"
          onChange={(e) => setFile(e.target.files[0])}
        />
        <label
          htmlFor="file-upload"
          className={`filepicker-label`}
          style={textStyle}
        >
          Upload File
        </label>
        <p className="mt-2 text-gray-500 text-xs truncate" style={textStyle}>
          {file === "" ? "No file selected" : file.name}
        </p>
      </div>
      <div className="mt-4 flex flex-wrap gap-3">
        <CostomButton
          type="outline"
          title="logo"
          handleClick={() => readFile("logo")}
          customStyles="text-xs"
        />
        <CostomButton
          type="filled"
          title="full"
          handleClick={() => readFile("full")}
          customStyles="text-xs"
        />
      </div>
    </div>
  );
};

export default FilePicker;
