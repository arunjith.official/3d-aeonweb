export default function Button({ children, white, outline, primary, block, size, black, ...rest }) {
    let buttonClasses = "bg-gray-400 text-black px-5 py-2 rounded-md cursor-pointer inline-flex items-center font-medium";
    
    if (white) {
      buttonClasses = "bg-white text-black px-5 py-2 rounded-md cursor-pointer inline-flex items-center font-medium";
      if (outline) {
        buttonClasses = "bg-transparent text-white border border-white px-5 py-2 rounded-md cursor-pointer inline-flex items-center font-medium";
      }
    }
    
    if (primary) {
      buttonClasses = `bg-${primary}text-white px-5 py-2 rounded-md cursor-pointer inline-flex items-center font-medium`;
      if (outline) {
        buttonClasses = `bg-transparent text-${primary} border border-${primary} px-5 py-2 rounded-md cursor-pointer inline-flex items-center font-medium`;
      }
    }
    
    if (block) {
      buttonClasses += " block w-full";
    }
    
    if (size === "l") {
      buttonClasses += " text-lg px-8 py-3";
    }
    
    if (black) {
      buttonClasses = "bg-black text-white px-5 py-2 rounded-md cursor-pointer inline-flex items-center font-medium";
    }
    
    return (
      <button className={buttonClasses} {...rest}>
        {children}
      </button>
    );
  }
  