import { useContext } from "react";
import { CartContext } from "./CartContext";

export default function ProductBox({ _id, title, description, price, images }) {
    const { addProduct } = useContext(CartContext);
    const url = '/product/' + _id;

    return (
        <div className="border border-gray-200 rounded-lg overflow-hidden">
            <a href={url} className="bg-white p-5 h-120 flex items-center justify-center border-b border-gray-200">
                <img src={images?.[0]} alt={title} className="max-w-full max-h-100" />
            </a>
            <div className="p-5">
                <a href={url} className="font-normal text-base text-black mb-5 inline-block hover:text-blue-500">
                    {title}
                </a>
                <div className="flex justify-between items-center">
                    <span className="text-lg font-semibold">₹{price}</span>
                    <button onClick={() => addProduct(_id)} className="bg-gray-400 text-black px-5 py-2 rounded-md cursor-pointer inline-flex items-center font-medium">Add to cart</button>
                </div>
            </div>
        </div>
    );
}
