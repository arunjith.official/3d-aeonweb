import Canvas from "./canvas"
import Home from "./pages/Home"
import Costomizer from "./pages/Costomizer"

function App() {
  return (
    <main className="app transition-all ease-in">
      <Home/>
      <Canvas/>
      <Costomizer/>
    </main>
  )
}

export default App
