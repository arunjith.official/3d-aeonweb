import { motion, AnimatePresence } from "framer-motion";
import { useSnapshot } from "valtio";
import state from "../store";
import {
  headContainerAnimation,
  headContentAnimation,
  headTextAnimation,
  slideAnimation,
} from "../config/motion";
import { CostomButton } from "../components";



const Home = () => {
  const snap = useSnapshot(state);
  return (
    <AnimatePresence>
      {snap.intro && (
        <motion.section className="home" {...slideAnimation("left")}>
          <motion.header
            className="flex items-center justify-center  text-white py-4 px-6 "
            {...slideAnimation("down")}
          >
            <img
              src="./threejs.png"
              alt="logo"
              className="w-8 h-8 object-contain"
            />
            <h1 className="pl-2 pb-1 text-lg poppins-bold">Aeon stores</h1>
          </motion.header>
          <motion.div className="home-content" {...headContainerAnimation}>
            <motion.div {...headTextAnimation}>
              <h1 className="head-text">
                Lets <br className="xl:block hidden" /> DO IT.
              </h1>
            </motion.div>
            <motion.div
              {...headContentAnimation}
              className="flex flex-col gap-5"
            >
              <p className="max-w-md font-normal text-white text-base">
                design your 3d shirts and shop with us
              </p>
            </motion.div>
            <div>
              <CostomButton
                type="filled"
                title="Costomize it"
                handleClick={() => (state.intro = false)}
                customStyles="w-fit px-4 py-2.5 font-bold text-sm"
              />
            </div>
          </motion.div>
          <motion.div>

          </motion.div>
        </motion.section>
      )}
    </AnimatePresence>
  );
};

export default Home;
